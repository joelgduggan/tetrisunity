﻿using UnityEngine;
using System.Collections;

public class BlockController : MonoBehaviour {

	private int currGridX, currGridY;
	
	private bool started = false;
	private SpriteRenderer spriteRenderer;
    private MeshRenderer shadowMeshRenderer;
    private Animator animator;
	
	public int GetCurrGridX() { return currGridX; }
	public int GetCurrGridY() { return currGridY; }
	public void SetCurrGridPosition(int x, int y) {
		currGridX = x;
		currGridY = y;
		UpdateTransform();
	}
	
	private Color tint;
	public Color GetColor() { return tint; }
	public void SetColor(Color c) {
		tint = c;
		if (started) {
			spriteRenderer.color = tint;
		}
	}

    public SpriteRenderer GetSpriteRenderer() { return spriteRenderer; }

    public float GetBlockSize() {
        return spriteRenderer.bounds.size.x;
    }

    void Awake() {
        spriteRenderer = GetComponent<SpriteRenderer>();
        animator = GetComponent<Animator>();
        shadowMeshRenderer = transform.Find("ShadowCaster").GetComponent<MeshRenderer>();
    }

	void Start () {
		started = true;
		spriteRenderer.color = tint;
		UpdateTransform();
	}

	private void UpdateTransform() {
		if (!started) return;
		
		transform.localPosition = new Vector2(currGridX, currGridY - GameGridController.GRID_HEIGHT) * GetBlockSize();
	}

    public void StartGlow() {
        animator.SetTrigger("GlowTrigger");
    }

    public void SetShadowCasterEnabled(bool enable) {
        shadowMeshRenderer.enabled = enable;
    }
}
