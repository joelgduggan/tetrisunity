﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class InputController : MonoBehaviour {

	private class InputItem {
		public string name;
		public bool allowRepeat;
		public float initRepeatDelay;
		public float repeatInterval;
		public Action inputHandler;

		public float lastDownTime;
		public float lastExecTime;
	}

	private List<InputItem> inputItemList = new List<InputItem>();

	void Start () {

	}
	
	void Update () {

		foreach (InputItem item in inputItemList) {

			if (Input.GetButton(item.name)) {
				
				bool moveNow = false;
				float timeNow = Time.time;
				
				if (Input.GetButtonDown(item.name)) {
					item.lastDownTime = timeNow;
					moveNow = true;
				}
				else if (item.allowRepeat && timeNow - item.lastDownTime > item.initRepeatDelay && timeNow - item.lastExecTime > item.repeatInterval) {
					moveNow = true;
				}
				
				if (moveNow) {
					item.lastExecTime = timeNow;
					item.inputHandler();
				}
			}
		}
	}

	public void addInput(string name, bool allowRepeat, float initRepeatDelay, float repeatInterval, Action inputHandler) {
		InputItem item = new InputItem();
		item.name = name;
		item.allowRepeat = allowRepeat;
		item.initRepeatDelay = initRepeatDelay;
		item.repeatInterval = repeatInterval;
		item.inputHandler = inputHandler;
		inputItemList.Add(item);
	}
}
