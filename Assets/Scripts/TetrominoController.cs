﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TetrominoController : MonoBehaviour {

	private TetrominoShape shape;
	private BlockController[] blocks = new BlockController[4];
	private BlockController[] shadowBlocks = new BlockController[4];
	private int currGridX, currGridY;					// position of the top-left corner of bounding box
	private int currRotationIndex;
    private int freeFallIterations;
    private bool isNextTetronimo;
	
	public BlockController[] GetBlocks() { return blocks; }
    public int GetFreeFallIterations() { return freeFallIterations; }
	
	public void Create(TetrominoShape shape) {
		this.shape = shape;
        this.freeFallIterations = 0;
        this.isNextTetronimo = false;
		
		for (int i = 0; i < blocks.Length; i++) {
			blocks[i] = Instantiate(GameGridController.instance.blockPrefab).GetComponent<BlockController>();
			blocks[i].transform.parent = transform;
			blocks[i].SetColor(GetTetrominoShapeColor(shape));

			shadowBlocks[i] = Instantiate(GameGridController.instance.blockPrefab).GetComponent<BlockController>();
			shadowBlocks[i].transform.parent = transform;
			shadowBlocks[i].SetColor(GetTetrominoShapeColor(shape).WithAlpha(0.2f));
            shadowBlocks[i].SetShadowCasterEnabled(false);
		}
		UpdateTransform();
	}

    public void SetIsNextTetronimo(bool value) {
        this.isNextTetronimo = value;
        SetShadowBlocksVisibility(!isNextTetronimo);
    }

    private void SetShadowBlocksVisibility(bool visible) {
        for (int i = 0; i < shadowBlocks.Length; i++) {
			shadowBlocks[i].GetSpriteRenderer().enabled = visible;
		}
    }

    public void SetGridPosition(int x, int y) {
        this.currGridX = x;
        this.currGridY = y;
        UpdateTransform();
    }

    /// Returns the number of blocks wide the tetronimo is with its current rotation
    public int GetGridWidth() {
        int[,] coords = GetTetrominoShapeCoords(shape, currRotationIndex);
        int minX = int.MaxValue;
        int maxX = int.MinValue;
        for (int i = 0; i < coords.Length/2; i++) {
            minX = Mathf.Min(minX, coords[i,0]);
            maxX = Mathf.Max(maxX, coords[i,0]);
        }
        return (maxX - minX + 1);
    }

    public float GetBlockSize() {
        if (blocks[0] == null) {
            Debug.Log("Called GetBlockSize() before tetronimo created");
            return 0;
        }
        return blocks[0].GetBlockSize();
    }

    public Rect GetBoundingBox() {
        float minX = int.MaxValue;
        float maxX = int.MinValue;
        float minY = int.MaxValue;
        float maxY = int.MinValue;
        for (int i = 0; i < blocks.Length; i++) {
            minX = Mathf.Min(minX, blocks[i].transform.position.x);
            maxX = Mathf.Max(maxX, blocks[i].transform.position.x);
            minY = Mathf.Min(minY, blocks[i].transform.position.y);
            maxY = Mathf.Max(maxY, blocks[i].transform.position.y);
        }
        float blockSize = GetBlockSize();
        return new Rect(minX, minY, maxX-minX+blockSize, maxY-minY+blockSize);
    }

    public Color GetColor() {
        return GetTetrominoShapeColor(shape);
    }

	public bool MoveLeft() {
		return MoveOrRotateIfPossible(-1, 0, 0);
	}

	public bool MoveRight() {
		return MoveOrRotateIfPossible(1, 0, 0);
	}

	public bool MoveDown(bool isFreeFall) {
        if (MoveOrRotateIfPossible(0, -1, 0)) {
            if (isFreeFall)
                freeFallIterations++;
            return true;
        }
        return false;
	}

	public bool Rotate(bool clockwise) {
		return MoveOrRotateIfPossible(0,  0, clockwise ? -1 : 1) ||
               MoveOrRotateIfPossible(1,  0, clockwise ? -1 : 1) ||
               MoveOrRotateIfPossible(2,  0, clockwise ? -1 : 1) ||
               MoveOrRotateIfPossible(-1, 0, clockwise ? -1 : 1);
	}

	/// Returns true if the move or rotation can be made succesfully
	private bool CanMoveOrRotationBeMade(int xOffset, int yOffset, int rotationOffset) {

		int newGridX = currGridX + xOffset;
		int newGridY = currGridY + yOffset;
		int newRotationIndex = currRotationIndex + rotationOffset;
		
		// get list of all occupied spaces
		BlockController[,] allBlocks = GameGridController.instance.GetLandedBlocksList();
		
		int[,] coords = GetTetrominoShapeCoords(shape, newRotationIndex);
		
		for (int i = 0; i < coords.Length/2; i++) {
			int x = coords[i,0] + newGridX;
			int y = coords[i,1] + newGridY;
			
			// check boundary
			if (x < 0 || x >= GameGridController.GRID_WIDTH || y < 0) return false;
			
			// check if space is occupied by a landed block
			if (y < GameGridController.GRID_HEIGHT && allBlocks[y,x] != null) return false;
		}

		return true;
	}


	/// Returns true if move was made
	private bool MoveOrRotateIfPossible(int xOffset, int yOffset, int rotationOffset) {

		if (CanMoveOrRotationBeMade(xOffset, yOffset, rotationOffset)) {
			currGridX += xOffset;
			currGridY += yOffset;
			currRotationIndex += rotationOffset;
			UpdateTransform();
			return true;
		}
		return false;
	}
	
	public void UpdateTransform() {

		// find the y-postion of the shadow blocks
		int shadowGridY = currGridY;
		while (shadowGridY > 0 && CanMoveOrRotationBeMade(0, shadowGridY - currGridY - 1, 0)) {
			shadowGridY--;
		}

		int[,] coords = GetTetrominoShapeCoords(shape, currRotationIndex);
		for (int i = 0; i < blocks.Length; i++) {

			blocks[i].SetCurrGridPosition(currGridX + coords[i,0],
										  currGridY + coords[i,1]);
			
			shadowBlocks[i].SetCurrGridPosition(currGridX   + coords[i,0],
			                              		shadowGridY + coords[i,1]);
		}
	}

	
	
	public enum TetrominoShape {
		I, J, L, O, S, T, Z
	};
	
	public static TetrominoShape GetRandomTetromino() {
		return (TetrominoShape)(Random.value * 7);
	}

	public static int[,] GetTetrominoShapeCoords(TetrominoShape s, int rotationIndex) {
		int r = rotationIndex % 4;
		switch (s) {
			case TetrominoShape.O:
				return new int[,] { {-1,0},{0,0},{-1,-1},{0,-1} };
			case TetrominoShape.I:
				if 	 (r%2 == 0)  return new int[,] { {-2,0},{-1,0},{0,0},{1,0} };
				else 		  	 return new int[,] { {0,1},{0,0},{0,-1},{0,-2} };
			case TetrominoShape.S:
				if   (r%2 == 0) return new int[,] { {0,0},{1,0},{-1,-1},{0,-1} };
				else         	return new int[,] { {0,0},{0,1},{1,0},{1,-1} };
			case TetrominoShape.Z:
				if   (r%2 == 0) return new int[,] { {0,0},{-1,0},{0,-1},{1,-1} };
				else  			return new int[,] { {0,0},{1,1},{1,0},{0,-1} };
			case TetrominoShape.L:
				if      (r == 0) return new int[,] { {-1,0},{0,0},{1,0},{-1,-1} };
				else if (r == 1) return new int[,] { {0,1},{0,0},{0,-1},{1,-1} };
				else if (r == 2) return new int[,] { {-1,0},{0,0},{1,0},{1,1} };
				else   			 return new int[,] { {-1,1},{0,1},{0,0},{0,-1} };
			case TetrominoShape.J:
				if      (r == 0) return new int[,] { {-1,0},{0,0},{1,0},{1,-1} };
				else if (r == 1) return new int[,] { {0,1},{0,0},{0,-1},{1,1} };
				else if (r == 2) return new int[,] { {-1,0},{0,0},{1,0},{-1,1} };
				else   			 return new int[,] { {-1,-1},{0,1},{0,0},{0,-1} };
			case TetrominoShape.T:
				if      (r == 0) return new int[,] { {-1,0},{0,0},{1,0},{0,-1} };
				else if (r == 1) return new int[,] { {0,1},{0,0},{0,-1},{1,0} };
				else if (r == 2) return new int[,] { {-1,0},{0,0},{1,0},{0,1} };
				else   			 return new int[,] { {-1,0},{0,1},{0,0},{0,-1} };
			default:
				return null;
		}
	}
	
	public static Color GetTetrominoShapeColor(TetrominoShape s) {
		switch (s) {
			case TetrominoShape.I: return Color.red;
			case TetrominoShape.J: return Color.yellow;
			case TetrominoShape.L: return Color.magenta;
            case TetrominoShape.O: return new Color(0.2f, 0.4f, 1f);
			case TetrominoShape.S: return Color.cyan;
			case TetrominoShape.T: return Color.green;
			case TetrominoShape.Z: return new Color(1.0f, 0.5f, 0f);
		}
		return Color.white;
	}
}
