﻿using UnityEngine;
using System.Collections;

public static class ExtensionMethods {

    /// <summary>
    /// Returns a new color with the r,g,b values from /color/ and an alpha value of /newAlpha/.
    /// </summary>
    /// <param name="color"></param>
    /// <param name="newAlpha"></param>
	public static Color WithAlpha(this Color color, float newAlpha) {
		return new Color(color.r, color.g, color.b, newAlpha);
	}

    /// <summary>
    /// Transforms /position/ from screen space into world space (ensuring that z value is 0).
    /// </summary>
    /// <param name="camera"></param>
    /// <param name="position"></param>
    public static Vector3 ScreenToWorldPoint2D(this Camera camera, Vector3 position) {
        Vector3 point = camera.ScreenToWorldPoint(position);
        point.z = 0;
        return point;
    }

    public static Vector3 Midpoint(this Vector3 a, Vector3 b) {
        return a + (b-a)/2;
    }
}
