﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class GameGridController : MonoBehaviour {

	public const int GRID_WIDTH = 10;
	public const int GRID_ROWS_OFFSCREEN = 4;
    public const int GRID_ROWS = 20;
    public const int GRID_HEIGHT = GRID_ROWS + GRID_ROWS_OFFSCREEN;

	public const int SPAWN_X = 6;
	public const int SPAWN_Y = 20;

	public const float KEY_REPEAT_INIT_DELAY = 0.3f;
	public const float KEY_REPEAT_INTERVAL = 0.06f;

    public bool DEBUG_EASY_PIECES = false;

	public GameObject blockPrefab;
	public GameObject tetrominoPrefab;
    public GameObject blockExplodePrefab;
    public GameObject hardDropTrailPrefab;
	public GameObject inputControllerObject;
    public GameObject levelTextObject;
    public GameObject linesTextObject;
    public GameObject scoreTextObject;
    public Transform nextTetronimoTransform;

	public static GameGridController instance = null;
	

	private BlockController[,] landedBlocks = new BlockController[GRID_HEIGHT+GRID_ROWS_OFFSCREEN, GRID_WIDTH];
    public BlockController[,] GetLandedBlocksList() { return landedBlocks; }

    private TetrominoController nextTetromino;
	private TetrominoController currTetromino;
	
	private float timeSinceLastTick = 0;
	
	private bool removingClearedLines = false;
	private bool[] whichLinesCleared = new bool[GRID_HEIGHT];

	private InputController inputController;

    private int totalLinesCleared = 0;
    private int currLevel = 1;
    private int score = 0;

	void Awake() {
		instance = this;	
	}

	void Start () {
		inputController = inputControllerObject.GetComponent<InputController>();

		inputController.addInput("Left", true, KEY_REPEAT_INIT_DELAY, KEY_REPEAT_INTERVAL, () => { DoLeftInput(); });
		inputController.addInput("Right", true, KEY_REPEAT_INIT_DELAY, KEY_REPEAT_INTERVAL, () => { DoRightInput(); });
		inputController.addInput("Down", true, KEY_REPEAT_INIT_DELAY, KEY_REPEAT_INTERVAL, () => { DoDownInput(); });
		inputController.addInput("Rotate Clockwise", false, 0, 0, () => { DoRotateClockwiseInput(); });
		inputController.addInput("Rotate CounterClockwise", false, 0, 0, () => { DoRotateCounterClockwiseInput(); });
		inputController.addInput("Drop", false, 0, 0, () => { DoDropInput(); });

        UpdateUIText();
        CreateNextTetromino();
        StartNextTetromino();
	}

	void DoLeftInput() {
		if (currTetromino) {
			currTetromino.MoveLeft();
		}
	}

	void DoRightInput() {
		if (currTetromino) {
			currTetromino.MoveRight();
		}
	}

	void DoRotateClockwiseInput() {
		if (currTetromino) {
			currTetromino.Rotate(true);
		}
	}

	void DoRotateCounterClockwiseInput() {
		if (currTetromino) {
			currTetromino.Rotate(false);
		}
	}

	void DoDownInput() {
		if (currTetromino) {
            if (!currTetromino.MoveDown(false)) {
                ResetTickTimer();
            }
		}
	}

	void DoDropInput() {
		if (currTetromino) {
            Rect startRect = currTetromino.GetBoundingBox();
			while (currTetromino.MoveDown(false)) {
			}
            Rect endRect = currTetromino.GetBoundingBox();
            StartHardDropTrailParticles(startRect, endRect);
			PerformTick();
		}
	}

	void Update () {
	
		if (!removingClearedLines) {
			timeSinceLastTick += Time.deltaTime;
			if (timeSinceLastTick > GetCurrentTickDuration()) {
				PerformTick();
			}
		}

	}

    private int GetEarnedLevel() {
        return Mathf.Min(10, Mathf.Max(1, 1 + ((totalLinesCleared-1) / 10)));
    }

    private int GetLinesRemaining() {
        return (currLevel * 10 + 1) - totalLinesCleared;
    }

    private float GetCurrentTickDuration() {
        return (11 - GetEarnedLevel()) * 0.05f;
    }


    private void CreateNextTetromino() {
		nextTetromino = Instantiate(tetrominoPrefab).GetComponent<TetrominoController>();
		nextTetromino.transform.parent = transform;

        TetrominoController.TetrominoShape newShape = TetrominoController.GetRandomTetromino();
        if (DEBUG_EASY_PIECES) newShape = (Random.value < 0.5) ? TetrominoController.TetrominoShape.I : TetrominoController.TetrominoShape.O;

		nextTetromino.Create(newShape);
        nextTetromino.SetIsNextTetronimo(true);
        nextTetromino.SetGridPosition(0, GRID_HEIGHT);
        nextTetromino.transform.position = Camera.main.ScreenToWorldPoint2D(nextTetronimoTransform.position);
        if (nextTetromino.GetGridWidth() == 3) {
            nextTetromino.transform.position -= new Vector3(nextTetromino.GetBlockSize()/2f, 0, 0);
        }
    }

	private void StartNextTetromino() {

        currTetromino = nextTetromino;
        currTetromino.transform.localPosition = Vector3.zero;
		currTetromino.SetGridPosition(SPAWN_X, SPAWN_Y);
        currTetromino.SetIsNextTetronimo(false);

        CreateNextTetromino();
	}
	
    private void ResetTickTimer() {
        timeSinceLastTick = 0;
    }

	private void PerformTick() {
	
		if (currTetromino) {
			
			if (!currTetromino.MoveDown(true)) {
			
				// couldn't move down, so it has "landed"
				BlockController[] newBlocks = currTetromino.GetBlocks();
				foreach (BlockController block in newBlocks) {
					block.transform.parent = transform;
					landedBlocks[ block.GetCurrGridY(), block.GetCurrGridX() ] = block;
                    block.StartGlow();
				}

                // give score for landed tetronimo
                score += (GRID_ROWS - currTetromino.GetFreeFallIterations()) * currLevel;
                UpdateUIText();

				// destroy the current tetromino 
				Destroy(currTetromino.gameObject);
				currTetromino = null;
				
				// check if any lines are cleared
				removingClearedLines = CheckForLineClears();
				
				// start next tetromino if no lines were cleared
				if (!removingClearedLines) {
					StartNextTetromino();
				} else {
					StartCoroutine(RemoveClearedLinesAnimated());	
				}
			}
		}
		else {
			StartNextTetromino();
		}
		
		timeSinceLastTick = 0;
	}
	
	// returns true if any lines are currently being cleared
	private bool CheckForLineClears() {
		
		bool anyLinesCleared = false;
		for (int y = 0; y < GRID_HEIGHT; y++) {
			whichLinesCleared[y] = true;
			for (int x = 0; x < GRID_WIDTH; x++) {
				if (landedBlocks[y,x] == null) {
					whichLinesCleared[y] = false;
					break;
				}
			}
			anyLinesCleared |= whichLinesCleared[y];
		}
		return anyLinesCleared;
	}

    private IEnumerator RemoveClearedLinesAnimated() {

        int numLinesCleared = 0;
        for (int y = GRID_HEIGHT-1; y >= 0; y--) {
            if (!whichLinesCleared[y]) {
                continue;
            }
            numLinesCleared++;

            // destroy all the blocks on this cleared line
            for (int x = 0; x < GRID_WIDTH; x++) {
                StartBlockExplosion(landedBlocks[y,x]);
                Destroy(landedBlocks[y,x].gameObject);
                landedBlocks[y,x] = null;
            }
            yield return new WaitForSeconds(0.25f);

            // move all the lines above this one down to fill in the now empty line
            DropBlocksOntoClearedLine(y);
            yield return new WaitForSeconds(0.25f);
        }

		removingClearedLines = false;
        UpdateLinesCleared(numLinesCleared);
	}

    private void DropBlocksOntoClearedLine(int yOfClearedLine) {
        for (int y = yOfClearedLine + 1; y < GRID_HEIGHT; y++) {
            for (int x = 0; x < GRID_WIDTH; x++) {
                if (landedBlocks[y,x] != null) {
                    int newY = y - 1;
                    landedBlocks[y,x].SetCurrGridPosition(x, newY);
                    landedBlocks[newY,x] = landedBlocks[y,x];
                    landedBlocks[y,x] = null;
                }
			}
        }
    }

    private void UpdateLinesCleared(int numCleared) {
        totalLinesCleared += numCleared;
        score += (100 << (numCleared - 1)) * currLevel;
        CheckForNewLevel();
        UpdateUIText();
    }

    private void StartBlockExplosion(BlockController blockToExplode) {
        ParticleSystem particleSystem = (ParticleSystem)Instantiate(blockExplodePrefab).GetComponent<ParticleSystem>();
		particleSystem.transform.parent = transform;
        particleSystem.transform.position = blockToExplode.transform.position;
        particleSystem.startColor = blockToExplode.GetColor();
        Destroy(particleSystem.gameObject, particleSystem.startLifetime + particleSystem.duration);
    }

    private void StartHardDropTrailParticles(Rect startRect, Rect endRect) {

        ParticleSystem particleSystem = (ParticleSystem)Instantiate(hardDropTrailPrefab).GetComponent<ParticleSystem>();
        particleSystem.transform.parent = transform;

        Rect trailRect = new Rect(startRect.x, endRect.yMin, startRect.width, startRect.y - endRect.y);
        particleSystem.transform.position = trailRect.center;
        particleSystem.transform.localScale = trailRect.size;

        particleSystem.startColor = currTetromino.GetColor();

        Destroy(particleSystem.gameObject, particleSystem.startLifetime + particleSystem.duration);
    }

    private IEnumerator StartFlashGlowDownEntireBoard() {

        for (int y = GRID_HEIGHT-1; y >= 0; y--) {
            
            bool anyBlocksInRow = false;
            for (int x = 0; x < GRID_WIDTH; x++) {
                if (landedBlocks[y, x] != null) { 
                    anyBlocksInRow |= landedBlocks[y,x] != null;
                    landedBlocks[y,x].StartGlow();
                }
            }

            if (anyBlocksInRow) {
                yield return new WaitForSeconds(0.1f);
            }
        }
	}

    private void UpdateUIText() {

        linesTextObject.GetComponent<Text>().text = GetLinesRemaining().ToString();
        levelTextObject.GetComponent<Text>().text = currLevel.ToString();
        scoreTextObject.GetComponent<Text>().text = score.ToString();
    }

    private void CheckForNewLevel() {

        if (currLevel != GetEarnedLevel()) {
            currLevel = GetEarnedLevel();
            UpdateUIText();
            StartCoroutine(StartFlashGlowDownEntireBoard());
        }
    }
}












